/*
  TODO(AARON):
  - Animate block across the screen in gameloop
  - Finish rendering square button and triangle button to a fixed region of the screen.
    - I'm assuming the pixel is uint32_t and RGBA. This could be false.
*/

#define _POSIX_C_SOURCE 199309L
#include <features.h>

#include <stdio.h>
#include <unistd.h>
#include <SDL.h>
#include <SDL_image.h>
#include <stdint.h>
#include <stdbool.h>
#include <x86intrin.h>
#include <time.h>

#define max(x,y) ((x) > (y) ? (x) : (y))

#define global
global unsigned int Width = 512;
global unsigned int Height = 512;

struct box {
  int X;
  int Y;
  int Width;
  int Height;
};

global struct box PlayerPos;
void
PutPixel(uint32_t *DisplayBuffer,
         unsigned int Width,
         unsigned int Height,
         unsigned int Pitch,
         unsigned int X,
         unsigned int Y,
         uint32_t Color);

int main(int ArgC, char **ArgV) {
  PlayerPos.X = Width / 2.0;
  PlayerPos.Y = Height / 2.0;
  PlayerPos.Width = 16;
  PlayerPos.Height = 16;

  if (SDL_Init(SDL_INIT_VIDEO) != 0) {
    fprintf(stderr, "\nUnable to initialize SDL: %s\n", SDL_GetError());
    return 1;
  }

  if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
    fprintf(stderr, "\nUnable to initialize SDL Image: %s\n", IMG_GetError());
    return 1;
  }

  SDL_Window *Window;
  SDL_Renderer *Renderer;
  SDL_Texture *Texture;
  uint32_t *DisplayBuffer;
  int DisplayBufferPitch;

  Window = SDL_CreateWindow("Ludum Dare 34", 100, 100, Width, Height, 0);
  if (Window == NULL) {
    fprintf(stderr, "\nCouldn't create window: %s\n", SDL_GetError());
    return 1;
  }

  Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_SOFTWARE);
  if (Renderer == NULL) {
    fprintf(stderr, "\nCouldn't create renderer: %s\n", SDL_GetError());
    return 1;
  }

  Texture = SDL_CreateTexture(Renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, Width, Height);
  if (Texture == NULL) {
    fprintf(stderr, "\nCouldn't create texture: %s\n", SDL_GetError());
    return 1;
  }

  SDL_Surface *SquareButton = IMG_Load("assets/square_button.png");
  if (!SquareButton) { fprintf(stderr, "Failed to load SquareButton: %s\n", IMG_GetError()); }
  SDL_Surface *TriangleButton = IMG_Load("assets/triangle_button.png");
  if (!TriangleButton) { fprintf(stderr, "Failed to load TriangleButton: %s\n", IMG_GetError()); }

  uint64_t PerfCountFrequency = SDL_GetPerformanceFrequency();

  bool Running = true;
  while(Running) {
    SDL_Event Event;
    uint64_t LastCounter = SDL_GetPerformanceCounter();
    uint64_t LastCycleCount = _rdtsc();

    while(SDL_PollEvent(&Event)) {
      switch(Event.type) {
        case SDL_QUIT: {
          Running = false;
          break;
        }

        case SDL_KEYDOWN:
          switch (Event.key.keysym.sym) {
            case SDLK_s: {
              fprintf(stdout, "Button 1\n");
              break;
            }
            case SDLK_k: {
              fprintf(stdout, "Button 2\n");
              break;
            }
          }
        case SDL_KEYUP: {
          if (Event.key.repeat == 0 && Event.key.keysym.sym == SDLK_ESCAPE) {
            Running = false;
            break;
          }
        }
      }
    }

    if (PlayerPos.X < 0 + PlayerPos.Width) { PlayerPos.X = 0 + PlayerPos.Width; }
    if (PlayerPos.X > Width - PlayerPos.Width) { PlayerPos.X = Width - PlayerPos.Width; }
    if (PlayerPos.Y < 0 + PlayerPos.Height) { PlayerPos.Y = 0 + PlayerPos.Height; }
    if (PlayerPos.Y > Height - PlayerPos.Height) { PlayerPos.Y = Height - PlayerPos.Height; }

    SDL_LockTexture(Texture, NULL, (void**)&DisplayBuffer, &DisplayBufferPitch);
    memset(DisplayBuffer, 0, Height * DisplayBufferPitch); // Zero out the entire texture

    unsigned int HalfWidth = PlayerPos.Width / 2.0;
    unsigned int HalfHeight = PlayerPos.Height / 2.0;
    for (int Y = 0; Y < PlayerPos.Height; ++Y) {
      for (int X = 0; X < PlayerPos.Width; ++X) {
        unsigned int XPos = PlayerPos.X - HalfWidth + X;
        unsigned int YPos = PlayerPos.Y - HalfHeight + Y;
        PutPixel(DisplayBuffer, Width, Height, DisplayBufferPitch, XPos, YPos, 0xFFFFFFFF);
      }
    }

    SDL_LockSurface(SquareButton);
    uint8_t *ReadRow = (uint8_t *)SquareButton->pixels;
    for (int Y = 0; Y < 16; ++Y) {
      uint8_t *ReadPixel = ReadRow + (Y * SquareButton->pitch);
      for (int X = 0; X < 16; ++X) {
        PutPixel(DisplayBuffer, Width, Height, DisplayBufferPitch, X, Y, (uint32_t)*ReadPixel++);
      }
    }
    SDL_UnlockSurface(SquareButton);

    SDL_UnlockTexture(Texture);
    SDL_RenderClear(Renderer);
    SDL_RenderCopy(Renderer, Texture, 0, 0);
    SDL_RenderPresent(Renderer);

    uint64_t EndCounter = SDL_GetPerformanceCounter();
    uint64_t CounterElapsed = EndCounter - LastCounter;

    double MsPerFrame = (((1000.0f * (double)CounterElapsed) / (double)PerfCountFrequency));
    double FPS = (double)PerfCountFrequency / (double)CounterElapsed;

    uint64_t EndCycleCount = _rdtsc();
    uint64_t CyclesElapsed = EndCycleCount - LastCycleCount;

    double MCPF = ((double)CyclesElapsed / (1000.0f / 1000.0f));

    fprintf(stdout, "%.02fms/f, %.02ff/s, %0.2fmc/f", MsPerFrame, FPS, MCPF);

    if (MsPerFrame < 33.33) {
      long Ms = 33.33 - MsPerFrame;
      struct timespec ToSleepFor;
      struct timespec Remainder;
      ToSleepFor.tv_sec = Ms / 1000;
      ToSleepFor.tv_nsec = (Ms % 1000) * 1000000;
      fprintf(stdout, "%9i nsec to sleep", ToSleepFor.tv_nsec);
      nanosleep(&ToSleepFor, &Remainder);
    }
    fprintf(stdout, "\n");

    LastCounter = EndCounter;
    LastCycleCount = EndCycleCount;
  }

  SDL_DestroyTexture(Texture);
  SDL_DestroyRenderer(Renderer);
  SDL_DestroyWindow(Window);

  IMG_Quit();
  SDL_Quit();

  return 0;
}

void
PutPixel(uint32_t *DisplayBuffer,
         unsigned int Width,
         unsigned int Height,
         unsigned int Pitch,
         unsigned int X,
         unsigned int Y,
         uint32_t Color) {

  uint32_t *DestRow = (uint32_t *)(((uint8_t*)DisplayBuffer) + (Pitch * Y));

  *(DestRow + X) = Color;
}
